<?php
/**
 * @package autoupdate
 * @version 1.6
 */
/*
Plugin Name: Autoupdate
Plugin URI: http://exertis.co.uk/
Description: Testing autoupdate, needed a plugin. This is not just a plugin, it symbolizes the hope and enthusiasm of an entire generation summed up in two words sung most famously by Louis Armstrong: Hello, Dolly. When activated you will randomly see a lyric from <cite>Hello, Dolly</cite> in the upper right of your admin screen on every page.
Author: actually Matt Mullenweg - Exertis
Version: 1.6
Author URI: http://exertis.co.uk/
*/

function hello_dolly_get_lyric() {
	/** These are the lyrics to Hello Dolly */
	$lyrics = "We know of an ancient radiation
That haunts dismembered constellations
A faintly glimmering radio station
While Frank Sinatra sings stormy Weather
The flies and spiders get along together
Cobwebs fall on an old skipping record
Beyond the suns that guard this roost
Beyond your flowers of flaming truth
Beyond your latest ad campaigns
An old man sits, collecting stamps
In a room all filled with Chinese lamps
He saves what others throw away
He says that he'll be rich someday
We know of an ancient radiation
That haunts dismembered constellations
A faintly glimmering radio station
We know of an ancient radiation
That haunts dismembered constellations
A faintly glimmering radio station
While Frank Sinatra sings Stormy Weather
The flies and spiders get along together
Cobwebs fall on an old skipping record";

	// Here we split it into lines
	$lyrics = explode( "\n", $lyrics );

	// And then randomly choose a line
	return wptexturize( $lyrics[ mt_rand( 0, count( $lyrics ) - 1 ) ] );
}

// This just echoes the chosen line, we'll position it later
function hello_dolly() {
	$chosen = hello_dolly_get_lyric();
	echo "<p id='dolly'>$chosen</p>";
}

// Now we set that function up to execute when the admin_notices action is called
add_action( 'admin_notices', 'hello_dolly' );

// We need some CSS to position the paragraph
function dolly_css() {
	// This makes sure that the positioning is also good for right-to-left languages
	$x = is_rtl() ? 'left' : 'right';

	echo "
	<style type='text/css'>
	#dolly {
		float: $x;
		padding-$x: 15px;
		padding-top: 5px;		
		margin: 0;
		font-size: 11px;
	}
	</style>
	";
}

add_action( 'admin_head', 'dolly_css' );

?>